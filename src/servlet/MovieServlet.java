package servlet;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.catalina.connector.Request;

import com.mysql.jdbc.PreparedStatement;

import model.Movie;
import persistence.MovieDTO;

/**
 * Servlet implementation class MovieServlet
 */
@WebServlet(urlPatterns = { "/movies", "/movies/*" })
public class MovieServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public MovieServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        String[] pieces = getUriPieces(request);

        int length = pieces.length;
        
        if (length == 3) {
         
            System.out.println("longitud 3");
            index(request, response);
        } else if (length > 3) {
            String piece = pieces[3];
            if (piece.equals("createMovie")) {
               
                create(request, response);
            } else if (piece.equals("delete")){
                delete(request, response);
            }}/* else {
                int id = 0;
                try {
                    id = Integer.parseInt(piece);
                } catch (Exception e) {
                    response.sendRedirect(request.getContextPath());
                }
                // show
                if (length == 4) {
                    show(request, response, id);
                } else {
                    piece = pieces[4];
                    if (piece.equals("edit")) {
                        edit(request, response, id);                        
                    } else if (piece.equals("delete")){
                        delete(request, response, id);                        
                    } else {
                        response.sendRedirect(".");
                    }
                }

            }
            System.out.println("longitud 4");
        }
        */
    }


    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
  
            create(request, response);
            
    
            } 
    private void index(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/view/movie/indexMovie.jsp");
        
        ArrayList<Movie> movies = null;
        try {
            MovieDTO dto = new MovieDTO();
            movies = dto.listAll();
        } catch (SQLException ex) {
            // TODO: handle exception
            System.out.println("Fallo en articles.all()");
            System.out.println("SQLException: " + ex.getMessage());
        }
        
        request.setAttribute("movies", movies);
        dispatcher.forward(request, response);
    }
    
    private String[] getUriPieces(HttpServletRequest request) {
        String uri = request.getRequestURI();
        System.out.println(uri);

        String[] pieces = uri.split("/");
        int i = 0;
        for (String piece : pieces) {
            System.out.println(i + ": " + piece);
            i++;
        }
        return pieces;
    }
    

    private void create(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    	   RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/view/movie/createMovie.jsp");
           dispatcher.forward(request, response);
           
        	   	  MovieDTO dto = new MovieDTO();
        	   	  
        	   	  try {
					dto.create(request, response);
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
        	   	  
        	   	response.sendRedirect("http://localhost:8080/Examen1702/movies");
          
           
    }
    
    private void delete(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
 	  
        
     	   	  MovieDTO dto = new MovieDTO();
     	   	  
     	   	String[] pieces = getUriPieces(request);
     	   
     	   	
     	   	  
     	   	  try {
					dto.delete(request, response,Integer.parseInt((pieces[pieces.length-1])));
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
     	   	  
        response.sendRedirect("http://localhost:8080/Examen1702/movies");
       
        
 }
    
    private void store(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/article/indexMovie.jsp");

        System.out.println("store");

        Movie movie = new Movie();

        movie.setTitle(request.getParameter("title"));
        movie.setDirector(request.getParameter("director"));
        
        movie.setYear(Integer.parseInt(request.getParameter("year")));
    
        
         ArrayList <Movie> list;
         HttpSession session = request.getSession(true);
         if (session.getAttribute("list") == null) {
             session.setAttribute("list", new ArrayList<Movie>());
         }
        
         list = (ArrayList<Movie>)session.getAttribute("list");
         list.add(movie);
         movie.setId(list.size()-1);
        
         System.out.println(movie);

        response.sendRedirect(request.getContextPath() + "/movies/indexMovie");
    }
    
    

}
