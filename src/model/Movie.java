package model;

import java.io.Serializable;

public class Movie implements Serializable{
    int id;
    String title;
	String director;
	int year = 0;
	
	
	public Movie(){
		
	}	
	
	public Movie( String title, String director, int year) {
        super();
      //  this.id = id;
        this.title = title;
        this.director = director;
        this.year = year;
    }

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDirector() {
		return director;
	}

	public void setDirector(String director) {
		this.director = director;
	}

	public int getYear() {
		return year;
	}

	public void setYear(int year) {
		this.year = year;
	}
}