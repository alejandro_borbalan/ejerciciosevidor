package persistence;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.mysql.jdbc.PreparedStatement;

import dbconnect.mysqlDAO;
import model.Movie;

public class MovieDTO extends mysqlDAO{
	
	

    public ArrayList <Movie> listAll() throws SQLException{
        ArrayList<Movie> movies = new ArrayList<Movie>();
        java.sql.PreparedStatement stmt = null;
        ResultSet rs = null;

        String sql = "SELECT * FROM movies";

        try {
            stmt = connection.prepareStatement(sql);
            rs = stmt.executeQuery();
            System.out.println("consulta realizada");
            while (rs.next()) {
                Movie movie = new Movie();
                movie.setId(rs.getInt("id"));
                movie.setTitle(rs.getString("title"));
                movie.setDirector(rs.getString("director"));
                movie.setYear(rs.getInt("year"));
                movies.add(movie);
            }
        } catch (SQLException ex) {
            // handle any errors
            System.out.println("SQLException: " + ex.getMessage());
            System.out.println("SQLState: " + ex.getSQLState());
            System.out.println("VendorError: " + ex.getErrorCode());
        } finally {
            if (stmt != null) {
                stmt.close();
            }
        }             
        
        
        return movies;
    }
    
    public Movie get(int id) {
        Movie movie = new Movie();
        PreparedStatement stmt = null;
        ResultSet rs = null;

        String sql = "SELECT * FROM articles WHERE id = ?";

        try {
            stmt = (PreparedStatement) connection.prepareStatement(sql);
            stmt.setInt(1, id);
            rs = stmt.executeQuery();
            System.out.println("consulta realizada");
            while (rs.next()) {
           
                movie.setId(rs.getInt("id"));
              
               
            }
        } catch (SQLException ex) {
            // handle any errors
            System.out.println("SQLException: " + ex.getMessage());
            System.out.println("SQLState: " + ex.getSQLState());
            System.out.println("VendorError: " + ex.getErrorCode());
        } finally {
            if (stmt != null) {
                try {
                    stmt.close();
                } catch (Exception e) {}
            }
        }             

        return movie;
        
    }
    
    public void create(HttpServletRequest request, HttpServletResponse response) throws SQLException{
    	
    	Movie movie = new Movie(request.getParameter("title"),request.getParameter("director"),Integer.parseInt(request.getParameter("year")));
        java.sql.PreparedStatement stmt = null;
        String sql = "INSERT INTO movies (title,director,year) VALUES (?, ? , ?)";
        
        stmt = connection.prepareStatement(sql);
        
        stmt.setString(1,movie.getTitle());
        stmt.setString(2,movie.getDirector());
        stmt.setInt(3,movie.getYear());
        
        stmt.executeUpdate();
        

    	
    }
    
    
    public void delete(HttpServletRequest request, HttpServletResponse response,int id) throws SQLException{
    	
    	java.sql.PreparedStatement stmt = null;
        String sql = "DELETE FROM movies WHERE id = ?";
        System.out.println(id);
        stmt = connection.prepareStatement(sql);
        
        stmt.setInt(1,id);
      
        
        stmt.executeUpdate();
        

    	
    }

}
