<%@ include file = "../header.jsp" %>

<%! List <Movie> movies;%>

<%
  movies = (List<Movie>)request.getAttribute("movies");
  if (movies == null) {
      movies = new ArrayList<Movie>();
  }
%>

<table>
<tr>
	
	<th>T�tulo</th>
	<th>Director</th>
	<th>Year</th>
	<th></th>
</tr>
<% for (Movie item : movies) {%>
  <tr>

  <td><%= item.getTitle() %></td>
  <td><%= item.getDirector() %></td>
  <td><%= item.getYear() %></td>
  <td>
  	<a href="<%= request.getContextPath() %>/movies/<%= item.getId() %>">ver</a>
  	<a href="<%= request.getContextPath() %>/movies/delete/<%= item.getId() %>">borrar</a>
  </td>
  </tr>
<% } %>
</table>


<%@ include file = "../footer.jsp" %>